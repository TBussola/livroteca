const mongoose = require("mongoose")
const Schema = mongoose.Schema;

const Livros = new Schema({
    nome: {
        type: String,
        required: true
    },
    descricao: {
        type: String,
        require: true
    },
    categoria: {
        type: String,
        require: true
    },
    date: {
        type: Date,
        default: Date.now()
    }
})

mongoose.model("livros", Livros)