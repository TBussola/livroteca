const mongoose = require("mongoose")
const Schema = mongoose.Schema;

const Usuarios = new Schema({
    nome: {
        type: String,
        required: true
    },
    idade: {
        type: Number,
        require: true
    },
    dataCadastro: {
        type: Date,
        default: Date.now()
    }
})

mongoose.model("usuario", Usuarios)