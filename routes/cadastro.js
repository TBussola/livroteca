const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
require("../models/Usuario")
const Usuario = mongoose.model("usuario")

router.get('/',(req, res) =>{
    res.render("livroteca/index")
})


router.get("/usuario/add", (req, res) => {
    res.render("livroteca/addusuario")
})

router.post("/usuario/novo", (req, res) =>{

    var erros = [];
    
    if(!req.body.nome || typeof req.body.nome == undefined || req.body.nome == null) {
        erros.push({texto:"Nome inválido"})
    }

    if(req.body.nome.length < 2){
        erros.push({texto: "Nome do usuario é muito pequeno"})
    }

    if(erros.length > 0) {
        res.render("livroteca/addusuario" , {erros: erros})
    } else {      
        const novoUsuario = {
            nome: req.body.nome
        }
    
        new Usuario(novoUsuario).save().then(() => {
            req.flash("success_msg", "Usuario adicionado com sucesso!")
            res.redirect("/livroteca/livros")
        }).catch((err) => {
            req.flash("error_msg", "Houve um erro ao salvar o usuario, tente novamente!")
            res.redirect("/livroteca")
        })
    }
})

router.get("/teste", (req, res) =>{
    res.send("Isso é um teste")
})


module.exports = router;