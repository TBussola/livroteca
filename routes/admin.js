const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
require("../models/Livro")
const Livro = mongoose.model("livro")

router.get('/',(req, res) =>{
    res.render("livroteca/index")
})

router.get("/posts", (req, res) =>{
    res.send("Página de posts")
})

router.get("/livros", (req, res) =>{
    Livro.find().sort({date: 'desc'}).then((livros) => {
        res.render("livroteca/livros", {livros: livros})
    }).catch((err) => {
        req.flash("error_msg", "Houve um erro ao listar os livros")
        res.redirect("/livroteca")
    })
    
})

router.get("/livros/add", (req, res) => {
    res.render("livroteca/addlivros")
})

router.post("/livros/novo", (req, res) =>{

    var erros = [];
    
    if(!req.body.nome || typeof req.body.nome == undefined || req.body.nome == null) {
        erros.push({texto:"Nome inválido"})
    }

    if(req.body.nome.length < 2){
        erros.push({texto: "Nome do livro é muito pequeno"})
    }

    if(erros.length > 0) {
        res.render("livroteca/addlivros" , {erros: erros})
    } else {      
        const novoLivro = {
            nome: req.body.nome,
            slug: req.body.slug
        }
    
        new Livro(novoLivro).save().then(() => {
            req.flash("success_msg", "Livro adicionado com sucesso!")
            res.redirect("/livroteca/livros")
        }).catch((err) => {
            req.flash("error_msg", "Houve um erro ao salvar os livros, tente novamente!")
            res.redirect("/livroteca")
        })
    }
})

router.get("/teste", (req, res) =>{
    res.send("Isso é um teste")
})


module.exports = router;